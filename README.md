# Developer challenge

After pulling this project from repository, you should go to the project directory.

Then you should install dependencies:<br />
  `make node-install`

Now you can run the app:<br />
  `make start` or `make up` for foreground mode

*First start can takes time (docker will pull images).*

Open [http://localhost:3001](http://localhost:3001) to view app in the browser.

For another port using you need change NODE_PORT in `.env` file and restart containers:<br />
  `make restart`

For stop running containers:<br />
  `make stop`

