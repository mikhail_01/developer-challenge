ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
include $(ROOT_DIR)/.mk-lib/common.mk

-include .env
-include .env.local
export

DOCKER_COMPOSE_FILE := $(ROOT_DIR)/docker-compose.yml
DC_CMD := $(DOCKER_COMPOSE) -f $(DOCKER_COMPOSE_FILE)


.PHONY: bash
bash: ## bash node
	@$(DC_CMD) exec -u node node /bin/sh


.PHONY: up start stop restart status ps clean logs
up: ## Start all or c=<name> containers in foreground
	@$(DC_CMD) up $(c)

start: ## Start all or c=<name> containers in background
	@$(DC_CMD) up -d $(c)

stop: ## Stop all or c=<name> containers
	@$(DC_CMD) stop $(c)

restart: ## Restart all or c=<name> containers
	@$(DC_CMD) stop $(c)
	@$(DC_CMD) up -d $(c)

status: ## Show status of containers
	@$(DC_CMD) ps

ps: status ## Alias of status

clean: ## Clean all data
	@$(DC_CMD) down

logs: ## Show all or c=<name> logs of containers
	@$(DC_CMD) logs $(c)


.PHONY: node-watch-logs node-install
node-watch-logs: ## Watch node logs
	@${DC_CMD} logs -f node

node-install: ## Run npm install
	@echo 'Installing npm dependencies ...'
	@$(DC_CMD) run -u node -T --no-deps --rm node sh -c "npm install"
	@echo 'Success'


