import * as actions from './actions';
import { loadQueriesExternal } from '../data/queries';
import { loadChartsExternal } from '../data/charts';

export const initialState = {
  queries: loadQueriesExternal(),
  charts: loadChartsExternal(),
  dashboard: [],
}

export function reducer(state = initialState, action) {
  switch(action.type) {
    case actions.DEL_QUERY:
      return {
        ...state,
        dashboard: state.dashboard.filter(key => state.charts.find(chart => chart.query === key).query !== action.key),
        charts: state.charts.filter(chart => chart.query !== action.key),
        queries: state.queries.filter(query => query.key !== action.key),
      };
    case actions.UPD_QUERY:
      return {
        ...state,
        queries: state.queries.filter(query => query.key !== action.key).concat({...action.values, key: action.key}).sort((a, b) => a.key - b.key),
      };
    case actions.DEL_CHART:
      return {
        ...state,
        dashboard: state.dashboard.filter(key => key !== action.key),
        charts: state.charts.filter(chart => chart.key !== action.key),
      };
    case actions.UPD_CHART:
      return {
        ...state,
        charts: state.charts.filter(chart => chart.key !== action.key).concat({...action.values, key: action.key}).sort((a, b) => a.key - b.key),
      };
    case actions.ADD_CHART_TO_DASHBOARD:
      return {
        ...state,
        dashboard: state.dashboard.concat(~~action.key),
      };
    case actions.SET_CHARTS_TO_DASHBOARD:
      return {
        ...state,
        dashboard: action.keys.slice(),
      };
    default:
      return state;
  }
}

