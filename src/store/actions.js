export const DEL_QUERY = 'DEL_QUERY';
export const UPD_QUERY = 'UPD_QUERY';

export const DEL_CHART = 'DEL_CHART';
export const UPD_CHART = 'UPD_CHART';

export const ADD_CHART_TO_DASHBOARD = 'ADD_CHART_TO_DASHBOARD';
export const SET_CHARTS_TO_DASHBOARD = 'SET_CHARTS_TO_DASHBOARD';

export const delQuery = key => ({ type: DEL_QUERY, key });
export const updQuery = (key, values) => ({ type: UPD_QUERY, key, values });

export const delChart = key => ({ type: DEL_CHART, key });
export const updChart = (key, values) => ({ type: UPD_CHART, key, values });

export const addToDashboard = (key) => ({ type: ADD_CHART_TO_DASHBOARD, key});
export const setToDashboard = (keys) => ({ type: SET_CHARTS_TO_DASHBOARD, keys});
