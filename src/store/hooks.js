import { initialState, reducer } from './reducer';
import * as persistedReducer from './persisted-reducer';

export function useMyPersistedReducer() {
  return persistedReducer.useLocallyPersistedReducer(reducer, initialState, 'dev-challenge');
}

export function clearState() {
  return persistedReducer.clearState('dev-challenge');
}
