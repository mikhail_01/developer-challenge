import { useReducer, useEffect } from 'react';

export function useLocallyPersistedReducer(reducer, defaultState, storageKey, init = null) {
  const hookVars = useReducer(reducer, defaultState, (defaultState) => {
    const persisted = JSON.parse(localStorage.getItem(storageKey));
    return persisted !== null
      ? persisted
      : init !== null ? init(defaultState) : defaultState;
  });

  let state = hookVars[0];
  useEffect(() => {
    localStorage.setItem(storageKey, JSON.stringify(state))
  }, [storageKey, state]);

  return hookVars;
}

export function clearState(storageKey) {
  localStorage.removeItem(storageKey);
}