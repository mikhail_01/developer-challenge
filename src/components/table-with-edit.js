import React from 'react';
import { Button, Space, Typography, Table, Tag, Row, Col, Input, Popover, Popconfirm, Form } from 'antd';
import { PlusOutlined } from '@ant-design/icons';


export function TableWithEdit(props) {

  const {
    title,
    entityName,
    dataSource,
    renderQuery,
    deleteEntity,
    saveEntity,
    editComponent,
    canNotAddMessage,
    extendedDeleteMessage,
  } = props;

  const [filter, setFilter] = React.useState('');
  const [modalVisible, setModalVisible] = React.useState(false);
  const [modalTitle, setModalTitle] = React.useState();
  const [previewData, setPreviewData] = React.useState();
  const [editKey, setEditKey] = React.useState(0);
  const [form] = Form.useForm();

  function openModalForAdd() {
    setEditKey(0);
    form.resetFields();
    setPreviewData();
    setModalTitle(`New ${entityName}`);
    setModalVisible(true);
  }

  function openModalForEdit(key) {
    const row = {...dataSource.find(row => row.key === key)};
    setEditKey(key);
    row.tags = row.tags.join(', ');
    form.setFieldsValue(row);
    setPreviewData();
    setModalTitle(`Edit ${entityName}`);
    setModalVisible(true);
  }

  function saveRow(values) {
    let key = editKey;
    if (!key) {
      key = 1 + dataSource.reduce(
        (newKey, row) => newKey < row.key ? row.key : newKey,
        0
      );
    }
    values.tags = values.tags ? values.tags.split(/\W+/).filter(tag => tag.length > 0) : [];
    saveEntity(key, values);
    setModalVisible(false);
  }

  function groupRowsByFolders(rows) {
    let folders = new Set();
    rows.forEach(row => {
      folders.add(row.folder);
    });
    let result = [];
    let nextKey = -1;
    for (let folder of folders) {
      result.push({
        key: nextKey--,
        folder: folder,
        children: rows.filter(row => row.folder === folder).map(row => ({...row, folder: ''})),
      });
    }
    return result;
  }
  
  function filteredData() {
    const rows = dataSource.slice();
    if (filter === '') return groupRowsByFolders(rows);
    const findFirstLetters = (haystack, needle) => {
      for (let str of haystack) {
        if (str.toLowerCase().indexOf(needle) === 0) return true;
      }
      return false;
    }
    return groupRowsByFolders(
      rows.filter(row => findFirstLetters(row.name.split(' '), filter) || findFirstLetters(row.tags, filter))
    );
  }

  let columns = [
    {
      title: 'Folder',
      dataIndex: 'folder',
      key: 'folder',
      width: '20%',
    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: '25%',
      sorter: (a, b) => a.name > b.name ? 1 : (a.name === b.name ? 0 : -1),
    },
    {
      title: 'Tags',
      dataIndex: 'tags',
      key: 'tags',
      width: '10%',
      render: tags => (
        tags ?
        <Space direction="vertical">
          {tags.map(tag => (
            <Tag key={tag}>{tag}</Tag>
          ))}
        </Space>
        : <></>
      )
    },
    {
      title: 'Query',
      dataIndex: 'query',
      key: 'query',
      render: renderQuery,
    },
    {
      title: 'Actions',
      dataIndex: 'key',
      key: 'key',
      width: '10%',
      render: key => (
        key > 0 ?
        <Space direction="vertical">
          <Button size='small' onClick={e => openModalForEdit(key)}>Edit</Button>
          <Popconfirm
            title={'Are you sure?' + (extendedDeleteMessage||'')}
            onConfirm={e => deleteEntity(key)}
            okText='Yes'
            cancelText='No'
          >
            <Button danger size='small'>Delete</Button>
          </Popconfirm>
        </Space>
        : <></>
      ),
    },
  ];

  return (
    <>
      <Typography.Title>{title}</Typography.Title>
      <Row>
        <Col span={16}>
          <Row justify='end'>
            <Space style={{paddingBottom: '1em'}}>
              <Input placeholder='Type to filter name + tags...' onChange={e => setFilter(e.target.value)} allowClear={true} />
              <Popover title='¯\_(ツ)_/¯' content={<p>It’s not obvious what exactly to edit,<br />please use buttons in rows.</p>}>
                <Button disabled>Edit</Button>
              </Popover>
              {
                canNotAddMessage ?
                  <Popover title="Can't add" content={<p>{canNotAddMessage}</p>}>
                    <Button disabled icon={<PlusOutlined />} onClick={openModalForAdd} />
                 </Popover>
                : <Button icon={<PlusOutlined />} onClick={openModalForAdd} />
              }              
            </Space>
          </Row>
          <Table pagination={false} tableLayout='auto' dataSource={filteredData()} columns={columns} />
        </Col>
      </Row>
      {
        canNotAddMessage ?
          <></> :
          editComponent({
            ...props,
            title: modalTitle,
            visible: modalVisible,
            setVisible: setModalVisible,
            form,
            editKey,
            previewData,
            setPreviewData,
            saveEntity: saveRow
          })
      }
    </>
  );

}