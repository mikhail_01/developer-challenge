import React from 'react';
import { Button, Typography, Row, Col } from 'antd';
import { PlusOutlined } from '@ant-design/icons';

import { useMyPersistedReducer } from '../store/hooks';
import { PreviewChart } from './preview-chart';
import { SelectChart } from './select-chart';


export function Dashboard() {

  const [state, dispatch] = useMyPersistedReducer();
  const [selectChartVisible, setSelectChartVisible] = React.useState(false);
  
  function ChartsOnDashboard() {
    return state.dashboard.map(chartKey => {
      const chart = state.charts.find(chart => chart.key === ~~chartKey);
      if (!chart) return <></>;
      return (
        <Row key={chart.key} justify="center" style={{marginTop: '3em'}}>
          <Col span={8}>
            <PreviewChart previewData={{queryKey: chart.query, chartType: chart.type}}  />
          </Col>
        </Row>
      );
    })
  }
  
  function openSelectChart() {
    setSelectChartVisible(true);
  }

  return (
    <>
      <Typography.Title>Dashboard</Typography.Title>
        {ChartsOnDashboard(state)}
      <Row justify="center" style={{marginTop: '6em'}} >
        <Button type="primary" size="large" icon={<PlusOutlined />} onClick={openSelectChart} />
      </Row>
      <Row style={{marginTop: '6em'}}></Row>
      <SelectChart visible={selectChartVisible} visibleSetter={setSelectChartVisible} state={state} dispatch={dispatch} />
    </>
  );
}

