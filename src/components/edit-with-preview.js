import React from 'react';
import { Button, Space, Row, Col, Modal, Empty } from 'antd';


export function EditWithPreview(props) {

  const {
    formComponent,
    previewComponent,
    saveEntity,
    setVisible,
    updatePreview,
    form,
    setPreviewData,
  } = props;

  function hide() {
    setVisible(false);
    setPreviewData();
  }

  function onOk() {
    form
      .validateFields()
      .then(values => {saveEntity(values); hide();})
      .catch(info => {
        console.log('Validate Failed:', info);
      });
  }

  return (
    <Modal
      {...props}
      width='80%'
      footer={null}
      onCancel={hide}
    >
      <Row justify="space-around">
        <Col span={11}>
          {formComponent(props)}
        </Col>
        <Col span={11}>
          {previewComponent(props) || <Empty style={{marginTop: '10em'}} />}
        </Col>
      </Row>
      <Row justify="space-around">
        <Col span={11}>
          <Row justify='center'>
            <Space size="large">
              <Button size="large" key="submit" type="primary" onClick={onOk}>
                OK
              </Button>
              <Button size="large" key="back" onClick={hide}>Cancel</Button>
            </Space>
          </Row>
        </Col>
        <Col span={11}>
          <Button size="large" onClick={updatePreview}>Preview</Button>
        </Col>
      </Row>
    </Modal>
  );
}
