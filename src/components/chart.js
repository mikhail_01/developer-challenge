import React from 'react';

import { ChartReact } from './chart-react';


export function Chart({chartType, queryResult}) {

  function columnNamesFromObj(obj) {
    let columns = [];
    for(let key in obj) {
        if (key ==='key') continue;
        columns.push(key);
    }
    return columns;
  }

  function columnFromResult(result, columnName, withColumnName=false) {
    return result.map(row => withColumnName ? columnName + ' ' + row[columnName] : row[columnName]);
  }

  const columnNames = columnNamesFromObj(queryResult[0]);

  let data = {
    type: chartType,
    data: {
      labels: columnFromResult(queryResult, columnNames[0], true),
      datasets: [{
        label: columnNames[1],
        data: columnFromResult(queryResult, columnNames[1]),
      }],
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
          }
        }]
      }
    }
  };

  return <ChartReact data={data} />;
}