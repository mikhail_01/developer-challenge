import React from 'react';
import { Tree } from 'antd';

import { TableWithEdit } from './table-with-edit';
import { EditChart } from './edit-chart';
import { useMyPersistedReducer } from '../store/hooks';
import * as actions from '../store/actions';


export function Charts()
{
  const [state, dispatch] = useMyPersistedReducer();
  
  function saveChart(key, values) {
    dispatch(actions.updChart(key, values));
  }

  function deleteChart(key) {
    dispatch(actions.delChart(key));
  }

  function renderQuery(queryKey) {
    if (!queryKey) return <></>;
    const query = state.queries.find(query => query.key === queryKey);
    const treeData = [
      {
        title: query.name,
        key: 'name',
        children: [
          {
            title: query.query,
            key: 'query',
          }
        ],
      },
    ];
    return (
      <Tree blockNode={true} treeData={treeData} />
    );
  }

  return (
    <TableWithEdit
      title='Charts'
      entityName='chart'
      dataSource={state.charts}
      renderQuery={renderQuery}
      deleteEntity={deleteChart}
      saveEntity={saveChart}
      editComponent={EditChart}
      canNotAddMessage={state.queries.length === 0 ? "There are no queries for charts. Please add a query first.": undefined}
    />
  );
}
