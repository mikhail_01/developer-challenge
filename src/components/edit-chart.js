import React from 'react';

import { EditWithPreview } from './edit-with-preview';
import { ChartForm } from './chart-form';
import { PreviewChart } from './preview-chart';

export function EditChart(props) {

  const {
    title,
    form,
    setPreviewData,
  } = props;

  function updatePreview() {
    setPreviewData({queryKey: form.getFieldValue('query'), chartType: form.getFieldValue('type')});
  }

  return (
    <EditWithPreview
      {...props}
      title={title || 'Edit chart'}
      formComponent={ChartForm}
      previewComponent={PreviewChart}
      updatePreview={updatePreview}
    />
  );
}