import React from 'react';

import { TableWithEdit } from './table-with-edit';
import { EditQuery } from './edit-query';
import { useMyPersistedReducer } from '../store/hooks';
import * as actions from '../store/actions';


export function Queries()
{
  const [state, dispatch] = useMyPersistedReducer();

  function deleteQuery(key) {
    dispatch(actions.delQuery(key));
  }

  function saveQuery(key, values) {
    dispatch(actions.updQuery(key, values));
  }

  return (
    <TableWithEdit
      title='Queries'
      entityName='query'
      dataSource={state.queries}
      deleteEntity={deleteQuery}
      saveEntity={saveQuery}
      editComponent={EditQuery}
      extendedDeleteMessage=' Linked charts will be deleted too!'
    />
  );
}
