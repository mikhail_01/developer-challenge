import React from 'react';
import { Input, Form } from 'antd';


export function QueryForm(props) {

  const { form } = props;

  return (
    <Form
      form={form}
      layout='vertical'
    >
      <Form.Item
        name='folder'
        label='Folder'
        rules={[
          {
            required: true,
            message: 'Folder name',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name='name'
        label='Name'
        rules={[
          {
            required: true,
            message: 'Query name',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name='tags'
        label='Tags'
        rules={[
          {
            required: false,
            message: 'Tags',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name='query'
        label='Query'
        rules={[
          {
            required: true,
            message: 'Query in SQL',
          },
        ]}
      >
        <Input.TextArea rows={6} />
      </Form.Item>
    </Form>
  );
}
