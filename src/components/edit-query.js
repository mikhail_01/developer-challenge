import React from 'react';

import { EditWithPreview } from './edit-with-preview';
import { QueryForm } from './query-form';
import { PreviewQuery } from './preview-query';

export function EditQuery(props) {

  const {
    title,
    form,
    setPreviewData,
  } = props;

  function updatePreview() {
    setPreviewData({query: form.getFieldValue('query')});
  }

  return (
    <EditWithPreview
      {...props}
      title={title || 'Edit query'}
      formComponent={QueryForm}
      previewComponent={PreviewQuery}
      updatePreview={updatePreview}
    />
  );
}