import React from 'react';
import { Table } from "antd";
import { executeQueryExternal } from '../data/queries';

export function PreviewQuery({previewData}) {

  function prepareColumnsFromObject(obj) {
    let columns = [];
    for(let key in obj) {
        if (key ==='key') continue;
        columns.push({
            title: key,
            dataIndex: key,
            key: key,
        });
    }
    return columns;
  }
  if (!previewData) return undefined;
  const queryResult =  executeQueryExternal(previewData.query);
  return queryResult ? 
    <Table pagination={false} dataSource={queryResult} columns={prepareColumnsFromObject(queryResult[0])} /> :
    undefined;
}