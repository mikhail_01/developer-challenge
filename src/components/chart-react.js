import React from 'react';
import Chart from 'chart.js';


export class ChartReact extends React.Component {
  constructor(props) {
    super(props);
    this.chartRef = React.createRef();
  }

  componentDidMount() {
    this.myChart = new Chart(this.chartRef.current, this.props.data);
  }

  render() {
    if (this.chartRef.current) {
      this.myChart = new Chart(this.chartRef.current, this.props.data);
    }
    return (
      <canvas ref={this.chartRef} />
    );
  }
}
