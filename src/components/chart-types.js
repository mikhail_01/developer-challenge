export const LINE_CHART = 'line';
export const BAR_CHART = 'bar';
export const PIE_CHART = 'pie';
export const DOUGHNUT_CHART = 'doughnut';
export const POLAR_CHART = 'polarArea';
export const RADAR_CHART = 'radar';


export const types = [
  {
    value: LINE_CHART,
    name: 'Line',
  },
  {
    value: BAR_CHART,
    name: 'Bar',
  },
  {
    value: PIE_CHART,
    name: 'Pie',
  },
  {
    value: DOUGHNUT_CHART,
    name: 'Doughnut',
  },
  {
    value: POLAR_CHART,
    name: 'Polar Area',
  },
  {
    value: RADAR_CHART,
    name: 'Radar',
  },
];

