import React from 'react';
import { Button, Divider, Typography, Row, Col, message } from 'antd';

import { clearState } from '../store/hooks';


export function Main() {

    function clearStateBtn() {
        clearState();
        message.info("Queries, charts and dashboard were cleared to initial state");
    }

    return (
        <Row justify='center'>
            <Typography.Title>Developer challenge</Typography.Title>
            <Divider />
            <Col span={6}>
                <br /><Button type="primary" size="large" block href="/queries">Queries</Button><br />
                <br /><Button type="primary" size="large" block href="/charts">Charts</Button><br />
                <br /><Button type="primary" size="large" block href="/dashboard">Dashboard</Button><br />
                <Divider />
                <br /><Button size="large" block onClick={clearStateBtn} >Clear state</Button><br />
            </Col>
        </Row>
    );
  }
  