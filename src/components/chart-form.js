import React from 'react';
import { Input, Form, Select } from 'antd';

import { useMyPersistedReducer } from '../store/hooks';
import * as chartTypes from './chart-types';

const { Option } = Select;


export function ChartForm(props) {
  
  const { form, editKey } = props;
  const [state, ] = useMyPersistedReducer();
  const chart = state.charts.find(chart => chart.key === editKey);
  let query;
  let chartType;
  if (chart) {
    chartType = chart.type;
    query = state.queries.find(query => query.key === chart.query);
  } else {
    chartType = chartTypes.LINE_CHART;
  }
  if (!query) {
    query = state.queries[0];
  }

  return (
    <Form
      form={form}
      layout='vertical'
    >
      <Form.Item
        name='folder'
        label='Folder'
        rules={[
          {
            required: true,
            message: 'Folder name',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name='name'
        label='Name'
        rules={[
          {
            required: true,
            message: 'Chart name',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name='tags'
        label='Tags'
        rules={[
          {
            required: false,
            message: 'Tags',
          },
        ]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name='query'
        label='Query'
        rules={[
          {
            required: true,
            message: 'Query name',
          },
        ]}
        initialValue={query.key}
      >
        <Select>
          {state.queries.map(query => (
            <Option key={query.key} value={query.key}>{query.name}</Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item
        name='type'
        label='Type'
        rules={[
          {
            required: true,
            message: 'Chart type',
          },
        ]}
        initialValue={chartType}
      >
        <Select>
          {chartTypes.types.map(type => (
            <Option key={type.value} value={type.value}>{type.name}</Option>
          ))}
        </Select>
      </Form.Item>
    </Form>
  );
}
