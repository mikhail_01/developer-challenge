import React from 'react';
import { Button, Row, Modal, Tree, Space, Divider } from 'antd';

// import { useMyPersistedReducer } from '../store/hooks';
import * as actions from '../store/actions';


export function SelectChart({visible, visibleSetter, state, dispatch}) {

  // const [state, dispatch] = useMyPersistedReducer();
  const [checkedCharts, setCheckedCharts] = React.useState(state.dashboard.slice());

  function prepareChartsTree() {

    function groupChartsByFolders(charts) {
      let folders = new Set();
      charts.forEach(chart => {
        folders.add(chart.folder);
      });
      let result = [];
      let nextKey = -1;
      for (let folder of folders) {
        result.push({
          key: nextKey--,
          title: folder,
          children: charts.filter(chart => chart.folder === folder).map(chart => ({title: chart.name, key: chart.key})),
        });
      }
      return result;
    }

    return groupChartsByFolders(state.charts);
  }

  function onCheck(checkedKeys, info) {
    setCheckedCharts(checkedKeys.slice());
  }

  function onOk() {
    dispatch(actions.setToDashboard(checkedCharts.filter(key => key > 0)));
    visibleSetter(false);
  }


  return (
    <Modal
      width='20%'
      title='Add Chart'
      visible={visible}
      footer={null}
      onCancel={() => visibleSetter(false)}
    >
      <Tree
        checkable
        treeData={prepareChartsTree()}
        onCheck={onCheck}
        height={200}
        defaultExpandAll
        selectable={false}
        defaultCheckedKeys={checkedCharts}
      />
      <Divider />
      <Row justify='center'>
        <Space>
          <Button type="primary" size="large" key="ok" onClick={onOk}>OK</Button>
          <Button size="large" key="back" onClick={() => visibleSetter(false) }>Cancel</Button>
        </Space>
      </Row>
    </Modal>
  );
}
