import React from 'react';

import { Chart } from './chart';
import { executeQueryExternal } from '../data/queries';
import { useMyPersistedReducer } from '../store/hooks';


export function PreviewChart({previewData}) {

  const [state] = useMyPersistedReducer();

  if (!previewData) return undefined;

  const { queryKey, chartType } = previewData;

  if (!queryKey || !chartType) {
    return undefined;
  }
  let query = state.queries.find(query => query.key === queryKey);
  if (!query) {
    return undefined;
  }
  let queryResult = executeQueryExternal(query);
  if (!queryResult) {
    return undefined;
  }

  return <Chart chartType={chartType} queryResult={queryResult} />;
}