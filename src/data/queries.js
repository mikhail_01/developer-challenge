const fakeQueries = [
    {
      key: 1,
      folder: 'Benchmark',
      name: 'Picks per hour',
      tags: ['hourly', 'picks'],
      query: 'SELECT\n Hour, Count(Picks)\nFROM\n Picks\nGROUP BY\n Hour'
    },
    {
      key: 2,
      folder: 'Benchmark',
      name: 'Errors per hour',
      tags: ['hourly', 'errors'],
      query: 'SELECT Hour, Count(Errors) FROM Picks GROUP BY Hour'
    },
    {
      key: 3,
      folder: 'SKU',
      name: 'Picks per hour 2',
      tags: ['hourly', 'picks'],
      query: 'SELECT Hour, Count(Picks) FROM Picks GROUP BY Hour'
    },
  ];

  let fakeResponse = [
    {
      key: 1,
      'Hour': 0,
      'Count(Picks)': 15,
    },
    {
      key: 2,
      'Hour': 1,
      'Count(Picks)': 20,
    },
    {
      key: 3,
      'Hour': 2,
      'Count(Picks)': 8,
    },
    {
      key: 4,
      'Hour': 3,
      'Count(Picks)': 12,
    },
  ];
  
  export function loadQueriesExternal() {
    return fakeQueries;
  }
  
  export function executeQueryExternal(query) {
    if (!query) return undefined;
    return fakeResponse;
  }
  