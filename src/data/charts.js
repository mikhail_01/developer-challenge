import * as chartTypes from '../components/chart-types';

let fakeCharts = [
  {
    key: 1,
    folder: 'Benchmark',
    name: 'Picks per hour chart',
    tags: ['hourly', 'picks'],
    query: 1,
    type: chartTypes.BAR_CHART,
  },
  {
    key: 2,
    folder: 'Benchmark',
    name: 'Errors per hour chart',
    tags: ['hourly', 'errors'],
    query: 2,
    type: chartTypes.LINE_CHART,
  },
  {
    key: 3,
    folder: 'SKU',
    name: 'Picks per hour 2 chart',
    tags: ['hourly', 'picks'],
    query: 3,
    type: chartTypes.DOUGHNUT_CHART,
  },
];


export function loadChartsExternal() {
  // for (let chart of fakeCharts) {
  //   chart.query = queries.find(query => query.key === chart.query);
  // }
  return fakeCharts;
}
