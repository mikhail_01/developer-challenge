import './index.css';
import 'antd/dist/antd.css';

import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import ReactDOM from 'react-dom';

import { Main } from './components/main';
import { Queries } from './components/queries';
import { Charts } from './components/charts';
import { Dashboard } from './components/dashboard';


ReactDOM.render(
  <Router>
    <Route exact path='/' render={() => <Main />} />
    <Route exact path='/queries' render={() => <Queries />} />
    <Route exact path='/charts' render={() => <Charts />} />
    <Route exact path='/dashboard' render={() => <Dashboard />} />
  </Router>,
  document.getElementById('root')
)